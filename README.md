# Assignment 4

## Pokémon Trainer

### Dependencies
To install dependencies run in the root directory of the project:
```bash
npm install
```

### Hosting
The application is hosted using Gitlab Pages:
https://patrickph.gitlab.io/assignment4/

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

