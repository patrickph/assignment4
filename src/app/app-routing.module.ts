import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {LoginPage} from "./login/pages/login-page/login.page";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/catalogue'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'trainer',
    loadChildren: () => import('./trainer/trainer.module').then(m => m.TrainerModule)
  },
  {
    path: 'catalogue',
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule)
  }
];

@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
