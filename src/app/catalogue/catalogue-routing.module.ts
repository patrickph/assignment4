import {RouterModule, Routes} from "@angular/router";
import {LoginPage} from "../login/pages/login-page/login.page";
import {NgModule} from "@angular/core";
import {CataloguePage} from "./pages/catalogue-page/catalogue.page";

const routes: Routes = [
  {
    path: '',
    component: CataloguePage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports: [
    RouterModule
  ]
})
export class CatalogueRoutingModule {
}
