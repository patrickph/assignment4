import {NgModule} from "@angular/core";
import {CataloguePage} from "./pages/catalogue-page/catalogue.page";
import {CatalogueRoutingModule} from "./catalogue-routing.module";
import {CatalogueListComponent} from "./components/catalogue-list/catalogue-list.component";
import {PokemonItemComponent} from "./components/pokemon-item/pokemon-item.component";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    CataloguePage,
    CatalogueListComponent,
    PokemonItemComponent
  ],
  imports: [
    CatalogueRoutingModule,
    CommonModule,
  ]
})
export class CatalogueModule {
}
