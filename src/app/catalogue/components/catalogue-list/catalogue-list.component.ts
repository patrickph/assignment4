import {Component, OnInit, Output} from "@angular/core";
import {Pokemon, User} from "../../../models/user.model";
import {PokemonService} from "../../services/pokemon.service";

@Component({
  selector: 'app-catalogue-list',
  templateUrl: './catalogue-list.component.html',
  // styleUrls: ['./catalogue-list.component.css']
})
export class CatalogueListComponent implements OnInit{

  constructor(private readonly service: PokemonService) {
  }

  ngOnInit() {
    // Fetch list of pokémon
    this.service.fetchPokemon()
  }

  get pokemon$(): Pokemon[] {
    // Return list of pokémon
    return this.service.getPokemon()
  }

  onPokemonClicked(pokemon: Pokemon) {
    // Add pokémon to the current users stored pokémon, if not already stored
    let cur = localStorage.getItem('currentUser')
    if (!cur) return
    let user: User = JSON.parse(cur)

    let owned = false
    for (let p of user.pokemon) {
      if (p.name == pokemon.name) {
        owned = true
      }
    }

    if (owned) return

    // Update current user and user in stored users

    user.pokemon.push(pokemon)

    localStorage.setItem('currentUser', JSON.stringify(user))

    let arr = localStorage.getItem('users')
    if (!arr) return
    let users: User[] = JSON.parse(arr)

    for (let u of users) {
      if (u.username == user.username) {
        u.pokemon = user.pokemon
      }
    }
    localStorage.setItem('users', JSON.stringify(users))
  }

}
