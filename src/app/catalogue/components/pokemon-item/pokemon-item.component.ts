import {Component, Input} from "@angular/core";
import {Pokemon, User} from "../../../models/user.model";

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css']
})
export class PokemonItemComponent {

  @Input() pokemon: Pokemon | undefined;

  get hasPokemon(): boolean {
    // Return if the user has stored the pokémon or not
    let cur = localStorage.getItem('currentUser')
    if (!cur) return false
    let user: User = JSON.parse(cur)

    for (let owned of user.pokemon) {
      if (owned.name == this.pokemon?.name) {
        return true
      }
    }
    return false
  }
}
