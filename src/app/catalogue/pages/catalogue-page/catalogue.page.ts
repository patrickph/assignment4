import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage {

  constructor(private readonly router: Router) {
    // If there is no current user - send to login page
    let currentUser = localStorage.getItem('currentUser')
    if (!currentUser) {
      this.router.navigate(['login']);
    }
  }

  onLogOutClick() {
    // Clear current user, navigate to login page
    localStorage.setItem('currentUser', '')
    this.router.navigate(['login']);
  }

  onTrainerClick() {
    // Navigate to trainer page
    this.router.navigate(['trainer']);
  }
}
