import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {APIresult, Pokemon} from "../../models/user.model";


@Injectable({
  providedIn: "root"
})
export class PokemonService {

  private pokemon: Pokemon[] = []

  constructor(private readonly http: HttpClient) {
  }

  private url = 'https://pokeapi.co/api/v2/pokemon/'

  // Fetch the list of pokémon from the API
  fetchPokemon() {
      this.http.get<APIresult>(this.url)
        .subscribe(r => this.pokemon = this.parsePokemon(r.results))
  }

  // Parse the pokemon from API, add ID and update name to have first letter uppercase
  parsePokemon(results: any): Pokemon[] {
    let output: Pokemon[] = []
    for (let result of results) {

      let arr = result.url.split("/")
      let id = ''
      if (arr) {
        id = arr[arr.length - 2]
      }

      let name = result.name[0].toUpperCase() + result.name.slice(1)

      output.push({
        id: id,
        name: name,
        url: result.url
      })
    }
    return output
  }

  // Return array of pokémon
  getPokemon(): Pokemon[] {
    return this.pokemon;
  }
}
