import {Component, EventEmitter, Output} from "@angular/core";
import {User} from "../../models/user.model";


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['login-form.component.css']
})
export class LoginFormComponent {

  @Output() successful: EventEmitter<string> = new EventEmitter<string>();

  constructor() {

  }

  public username: string = ''

  public onLoginClick(): void {
    // Get users from local storage
    let users: any = localStorage.getItem('users')
    let curUser: User | null = null
    // If there are users already stored
    if (users) {
      users = JSON.parse(users)
      let exist = false
      for (let user of users) {
        // If the user already exist - update current user
        if (user.username == this.username) {
          exist = true
          curUser = user
        }
      }
      // If the user doesn't exist
      if (!exist) {
        // Create new user
        let newUser: User = {
          username: this.username,
          pokemon: []
        }
        // Add user to existing users, update current user
        users.push(newUser)
        curUser = newUser
      }
    } else {
      // No users exist, update existing users and current user
      let newUser: User = {
        username: this.username,
        pokemon: []
      }
      users = [newUser]
      curUser = newUser
    }
    localStorage.setItem('users', JSON.stringify(users))
    localStorage.setItem('currentUser', JSON.stringify(curUser))

    this.successful.emit(this.username)
  }
}
