import {NgModule} from "@angular/core";
import {LoginPage} from "./pages/login-page/login.page";
import {FormsModule} from "@angular/forms";
import {LoginRoutingModule} from "./login-routing.module";
import {LoginFormComponent} from "./components/login-form.component";

@NgModule({
  declarations: [
    LoginPage,
    LoginFormComponent
  ],
  imports: [
    FormsModule,
    LoginRoutingModule
  ]
})
export class LoginModule {
}
