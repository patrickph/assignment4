export interface User {
  username: string,
  pokemon: Pokemon[]
}

export interface Pokemon {
  id: string,
  name: string,
  url: string
}

export interface APIresult {
  "count": number,
  "next": string,
  "previous": string,
  "results": Pokemon[]
}
