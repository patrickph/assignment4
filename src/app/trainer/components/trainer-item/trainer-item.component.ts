import {Component, Input} from "@angular/core";
import {Pokemon} from "../../../models/user.model";

@Component({
  selector: 'app-trainer-item',
  templateUrl: './trainer-item.component.html',
  styleUrls: ['./trainer-item.component.css']
})
export class TrainerItemComponent {

  @Input() pokemon: Pokemon | undefined;

}
