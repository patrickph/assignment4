import {Component} from "@angular/core";
import {Pokemon, User} from "../../../models/user.model";


@Component({
  selector: 'app-trainer-list',
  templateUrl: './trainer-list.component.html'
})
export class TrainerListComponent {

  constructor() {
  }

  get pokemon$(): Pokemon[] {
    // Return the stored pokémon of the user
    let cur = localStorage.getItem('currentUser')
    if (!cur) return []
    let user = JSON.parse(cur)
    return user.pokemon
  }

  get username$(): string {
    // Return the username of the suer
    let cur = localStorage.getItem('currentUser')
    if (!cur) return ''
    let user = JSON.parse(cur)
    return user.username
  }

  onPokemonClicked(pokemon: Pokemon) {
    // For later functionality on click on the stored pokémon
  }
}
