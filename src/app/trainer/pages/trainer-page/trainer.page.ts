import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage {

  constructor(private readonly router: Router) {
    // If the user is not logged in - navigate to the login page
    let currentUser = localStorage.getItem('currentUser')
    if (!currentUser) {
      this.router.navigate(['login']);
    }
  }

  onCatalogueClick() {
    // Navigate to the catalougue page
    this.router.navigate(['catalogue']);
  }

  onLogOutClick() {
    // Log out the current user, navigate to the login page
    localStorage.setItem('currentUser', '')
    this.router.navigate(['login']);
  }
}
