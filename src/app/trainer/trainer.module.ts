import {NgModule} from "@angular/core";
import {TrainerRoutingModule} from "./trainer-routing.module";
import {TrainerPage} from "./pages/trainer-page/trainer.page";
import {TrainerListComponent} from "./components/trainer-list/trainer-list.component";
import {TrainerItemComponent} from "./components/trainer-item/trainer-item.component";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    TrainerPage,
    TrainerListComponent,
    TrainerItemComponent
  ],
  imports: [
    TrainerRoutingModule,
    CommonModule
  ]
})
export class TrainerModule {
}
